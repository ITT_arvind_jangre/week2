<?php
header("Content-Type: application/json; charset=UTF-8");
$_SESSION["user_id"] = "1";

if($_POST["task_name"]) {
    $data = array(
        ':user_id' =>$_SESSION['user_id'],
        ':task_details' =>trim($_POST["task_name"]),
        ':task_status' => 'no'
    );

    if(file_exists("data_base.json") == 0) {
        fopen("data_base.json", 'a+');
        }
   $curret_data = file_get_contents("data_base.json");
   $array_data = json_decode($curret_data, true);
   $array_data[] = $data;
   $final_data = json_encode($array_data);

    
    file_put_contents("data_base.json", $final_data);

}
    ?>