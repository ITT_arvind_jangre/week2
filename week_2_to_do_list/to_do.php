<?php
//index.php

$str = file_get_contents("data_base.json");
$str = rtrim($str, "\0");


$jsonData = stripslashes(html_entity_decode($str));
$json = json_decode(preg_replace('/[\x00-\x1F\x80-\xFF]/', '', $jsonData), true);
//var_dump(json_decode(stripslashes($json)));




?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>To do list</title>
    <script src="jquery.min.js"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" />
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

</head>

<body>

    <header>
        <h1>To-do list</h1>
    </header>
    <section>
        <form method="post" id="form_id">
            <span id="message"></span>
            <input type="text" name="task_name" id="task_name" class="form-control input-lg" autocomplete="off" placeholder="Title..." />
            <div class="input-group-btn">
                <button type="submit" name="submit" id="submit" class="btn btn-success btn-lg" ></button>
            </div>

            <div class="list-group">
                <?php
                // if(count($json) != 0) {
                $arr = '{":user_id":"1",":task_details":"hmmmmmmm",":task_status":"no"}';

                $style = "";
                $arr1 = json_decode($arr, true);


                foreach ($json as $obj) {
                    foreach ($obj as $key => $value) {


                        if ($value == 'yes') {
                            $style = 'text-decoration: line-through';
                        }
                        if ($key == ':task_details') {

                            echo '<a href="#" style="' . $style . '" class="list-group-item" id="list-group-item-' . '" data-id="' . '">' . $value . ' <span class="badge" data-id="' . '">X</span></a>';
                        }
                    }
                }


                //}
                ?>
            </div>


        </form>



    </section>
    <footer></footer>

    <script>
        $(document).ready(function() {
            $(document).on('submit', '#form_id', function(event) {
                    event.preventDefault();

                    if ($('#task_id').val() == '') {
                        $('#message').html('<div class"alert alert-danger">Enter task Details</div>');
                        return false;
                    } else {
                        $('#submit').attr('disabled', 'disabled');
                        $.ajax({
                            url: "add_task.php",
                            method: "POST",
                            data: $(this).serialize(),
                            success: function(data) {
                                $('#submit').attr('disabled', false);
                                $('#form_id')[0].reset();
                                $('.list-group').prepend(data);
                            }
                        })
                    }

                });
                $(document).on('click', '.badge', function() {
                    var task_list_id = $(this).data('id');
                    $.ajax({
                        url: "delete_task.php",
                        method: "POST",
                        data: {
                            task_list_id: task_list_id
                        },
                        success: function(data) {
                            $('#list-group-item-' + task_list_id).fadeOut('slow');
                        }
                    })
                })
            


        });
    </script>
</body>

</html>