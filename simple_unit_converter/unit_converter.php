<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" type="text/css" href="style.css" >
</head>

<body>


    <h1>Simple unit converter</h1>
    <section>
        <form method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>">
            <div>
                <h3>mass</h3>
                <select name="mass1" id="mass1_id">
                    <option value="kilos" selected>kilos</option>
                    <option value="pounds">pounds</option>
                </select>

                <select name="mass2" id="mass2_id">
                    <option value="kilos" selected>kilos</option>
                    <option value="pounds">pounds</option>
                </select>
                <input type="number" name="inp_mass1">

               
            </div>
            <br>
            <div>
                <h3>length</h3>
                <select name="length1" id="length1_id">
                    <option value="meters">meters</option>
                    <option value="yards">yards</option>
                </select>

                <select name="length2" id="length2_id">
                    <option value="meters">meters</option>
                    <option value="yards">yards</option>
                </select>
                <input type="number" name="inp_len1">

               
            </div>
            <br>
            <div>
                <h3>temperature</h3>
                <select name="temp1" id="temp1_id">
                    <option value="celsius">celsius</option>
                    <option value="Fahrenheit">fahrenheit</option>
                </select>

                <select name="temp2" id="temp2_id">
                    <option value="celsius">celsius</option>
                    <option value="Fahrenheit">fahrenheit</option>
                </select>
                <input type="number" name="inp_temp1">

            </div>
            <br>

            <input type="submit" name="submit" value="Convert">



        </form>
    </section>

    <div id="result">
    
    <?php
    $mass1 = $mass2 = $temp1 = $temp2 = $len1 = $len2 = "";
    $ans1 = 0;
    if ($_SERVER["REQUEST_METHOD"] == "POST") {
        
        if (isset($_POST["mass1"])) {
            if ($_POST["mass1"] == "kilos") {

                
                $mass1 = $_POST["inp_mass1"];
                if ($_POST["mass2"] == "kilos") {
                    $ans1 = $mass1;
                    
                }
                if ($_POST["mass2"] == "pounds") {
                    $ans1 = $mass1 * 2.20462;
                   
                }
            }
            if ($_POST["mass1"] == "pounds") {
                $mass1 = $_POST["inp_mass1"];
                if ($_POST["mass2"] == "pounds") {
                    $ans1 = $mass1;
                  
                } else {
                    $ans1 = $mass1 / 2.20462;
                   
                }
            }
            echo $ans1;
        }
        
        if (isset($_POST["length1"])) {
            if ($_POST["length1"] == "meters") {


                $mass1 = $_POST["inp_len1"];
                if ($_POST["length2"] == "meters") {
                    $ans1 = $mass1;
                    
                }
                if ($_POST["length2"] == "yards") {
                    $ans1 = $mass1 * 1.09361;
                    
                }
            }
            if ($_POST["length1"] == "yards") {
                $mass1 = $_POST["inp_len1"];
                if ($_POST["length2"] == "yards") {
                    $ans1 = $mass1;
                   
                } else {
                    $ans1 = $mass1 / 1.09361;
                   
                }
            }
            echo $ans1;
        }
        if (isset($_POST["temp1"])) {

            if ($_POST["temp1"] == "celsius") {


                $mass1 = $_POST["inp_temp1"];
                if ($_POST["temp2"] == "celsius") {
                    $ans1 = $mass1;
                    
                }else
                 {
                    $ans1 = ($mass1 * 9 / 5) + 32;
                    
                }
            }
            if ($_POST["temp1"] == "Fahrenheit") {
                $mass1 = $_POST["inp_temp1"];
                if ($_POST["temp2"] == "Fahrenheit") {
                    $ans1 = $mass1;
                    
                } else {
                    
                    $ans1 = ( $mass1 - 32 ) * 5 / 9;

                    
                }
            }
            echo $ans1;
        }


        
    } 




    ?>
    </div>
    <main>
      
    </main>
</body>

</html>